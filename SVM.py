import csv
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score, plot_confusion_matrix, precision_recall_curve, plot_precision_recall_curve, average_precision_score
from sklearn import svm, metrics, datasets
import matplotlib.pyplot as plt
from scipy import signal
from scipy.signal import find_peaks
from scipy.fftpack import fft
from scipy.stats import zscore
import pywt
from sklearn.feature_selection import RFE
from sklearn import preprocessing
from sklearn.pipeline import FeatureUnion, Pipeline
import sklearn.feature_selection

def Extract(lst):
    return [item[-1] for item in lst]
  

def wvlet(lst, wvfm):
    wvlsta=[]
    wvlstd=[]
    widths=np.arange(1,31)
    for i in lst:
        ca,cd = pywt.dwt(i, wvfm)
        wvlsta.append(ca)
        wvlstd.append(cd)
    return wvlsta

def RR(lst):
    rrlist = []
    for i in lst:
        # print(lst[i])
        # print(i)
        # print(lst[i].shape)
        peaks, _ = find_peaks(i, prominence=1, height=100)
        rr = np.diff(peaks)
        rr_corr = rr.copy()
        rr_corr[np.abs(zscore(rr)) > 1.21] = np.median(rr)
        mean_rr = np.mean(rr_corr)
        rrlist.append(rr)
    return rrlist

def HRV(rrlst):
    results = []
    for i in rrlst:
        # Mean RR
        mean_rr = np.mean(i)
        # RMSSD: take the square root of the mean square of the differences
        rmssd = np.sqrt(np.mean(np.square(np.diff(i))))
        # SDNN
        sdnn = np.std(i)
        # HR
        hr = 6000 / mean_rr
        # Mean HR
        mean_hr = 60 * 100 / mean_rr  # a bit clapped
        # STD HR
        std_hr = np.std(hr)
        # Min HR
        min_hr = np.min(hr)
        # Max HR
        max_hr = np.max(hr)
        # NNxx: sum absolute differences that are larger than 50ms
        nnxx = np.sum(np.abs(np.diff(i)) > 50) * 1
        # pNNx: fraction of nnxx of all rr-intervals
        pnnx = 100 * nnxx / len(i)

        results.append([mean_rr, rmssd, sdnn, mean_hr, std_hr, min_hr, max_hr, nnxx, pnnx])
    return results
    
def trainIt(features, label):
  X_train, X_test, y_train, y_test = train_test_split(features, label, test_size=0.3)

  x = pd.DataFrame(data=X_train)
  valid_indexes = ~x.isin([np.nan, np.inf, -np.inf]).any(1)
  x=x[valid_indexes]
  y_train = y_train[valid_indexes]

  xt = pd.DataFrame(data=X_test)
  valid_indexes = ~xt.isin([np.nan, np.inf, -np.inf]).any(1)
  xt=xt[valid_indexes]
  y_test = y_test[valid_indexes]
  
  return x, y_train, xt, y_test

def scaleIt(xvar):
  scaler = preprocessing.RobustScaler()
  x_scaled = scaler.fit_transform(xvar)

  return x_scaled

def makeRFE(xscaled, ytrain, Y_TE, X_TE):
  SVM1 = svm.SVC(kernel = 'linear')
  ftelim = RFE(SVM1)
  pipeline = Pipeline([
                     ('rfe_feature_selection', ftelim),
                     ('clf', SVM1)
                     ])
  pipeline.fit(xscaled, ytrain)
  y_pred = pipeline.predict(X_TE)

  support = pipeline.named_steps['rfe_feature_selection'].support_
  newcols = cols[support]

  #Evaluate
  print("Confusion matrix:", confusion_matrix(Y_TE, y_pred))
  print("Clf report:", classification_report(Y_TE, y_pred))
  print("Accuracy:", accuracy_score(Y_TE, y_pred))

  print(newcols)

def makeSVM(X, Y_T, kernel):

  newfts = [X[1], X[2], X[7], X[8]]
  newfts = np.array(newfts)
  newfts = np.array(newfts)
  print(newfts.shape)

  newSCALE_X = scaleIt(newfts)

  newload = np.transpose(newfts)
  print(newload.shape)

  trainX, trainy, testX, testy = trainIt(newload, Y_T)

  CLAF = svm.SVC(kernel=kernel,gamma=1,C=1)
  CLAF.fit(trainX, trainy)
  y_pred = CLAF.predict(testX)

  print("Confusion matrix:\n", confusion_matrix(testy, y_pred))
  print("Clf report:", classification_report(testy, y_pred))
  print("Accuracy:", accuracy_score(testy, y_pred)
  return CLAF, newfts, testX, testy



target = []
lis = []
data = []
labels = ['apnea', 'no apnea']

with open("drive/My Drive/ApneaData.csv") as csvDataFile:
    csvReader = csv.reader(csvDataFile)
    for line in csvReader:
        lis.append(line[0].split())  # create a list of lists

for i in range(1, len(lis)):
    data.append(list(map(int, lis[i])))

target = Extract(data)  # sleep apn or not

data = np.array(data)
target = np.array(target)

ca = wvlet(data,'rbio3.3')

peaks = RR(ca)

hrvs = HRV(peaks)

cols=['mean_rr', 'rmssd', 'sdnn', 'mean_hr', 'std_hr', 'min_hr', 'max_hr', 'nnxx', 'pnnx'] 
cols = np.array(cols)

X, Y_T, X_TE, Y_TE = trainIt(hrvs, target)


SCALE_X = scaleIt(X)
tSCALE_X = np.transpose(SCALE_X)

clf, feature, xte, yte = makeSVM(tSCALE_X, Y_T,'rbf')

plot_confusion_matrix(clf, xte, yte)

y_score = clf.decision_function(xte)

average_precision = average_precision_score(yte, y_score)

disp = plot_precision_recall_curve(clf, testX, testy)
disp.ax_.set_title('2-class Precision-Recall curve: '
                   'AP={0:0.2f}'.format(average_precision))